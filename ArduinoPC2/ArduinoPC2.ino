const byte numLEDs = 3;
byte ledPin[numLEDs] = {11, 12, 13};
unsigned long LEDinterval[numLEDs] = {0, 0, 0};
unsigned long prevLEDmillis[numLEDs] = {0, 0, 0};

const byte buffSize = 50;
char inputBuffer[buffSize];
const char startMarker = '<';
const char endMarker = '>';
byte bytesRecvd = 0;
boolean readInProgress = false;
boolean newDataFromPC = false;

char messageFromPC[buffSize] = {0};
int newledonoff = 0;
int newDAC0level = 0;
int newDAC1level = 0;
int DAC0level =0;
int DAC1level =0;

unsigned long curMillis;

unsigned long prevReplyToPCmillis = 0;
unsigned long replyToPCinterval = 1000;

//=============

void setup() {
  Serial.begin(115200);
  
    // flash LEDs so we know we are alive
  for (byte n = 0; n < numLEDs; n++) {
     pinMode(ledPin[n], OUTPUT);
     digitalWrite(ledPin[n], HIGH);
  }
  delay(500); // delay() is OK in setup as it only happens once
  
  for (byte n = 0; n < numLEDs; n++) {
     digitalWrite(ledPin[n], LOW);
  }
  
    // initialize DAC1,DAC2
  analogWriteResolution(10);
  pinMode(DAC0, OUTPUT);
  pinMode(DAC1, OUTPUT);
  
  
    // tell the PC we are ready
  Serial.println("<Arduino is ready>");
}

//=============

void loop() {
  curMillis = millis();
  getDataFromPC();
  updateFlashInterval();
  updateDAC0();
  updateDAC1();
  replyToPC();
  flashLEDs();
  flashDACs();
}

//=============

void getDataFromPC() {

    // receive data from PC and save it into inputBuffer
    
  if(Serial.available() > 0) {

    char x = Serial.read();

      // the order of these IF clauses is significant
      
    if (x == endMarker) {
      readInProgress = false;
      newDataFromPC = true;
      inputBuffer[bytesRecvd] = 0;
      parseData();
    }
    
    if(readInProgress) {
      inputBuffer[bytesRecvd] = x;
      bytesRecvd ++;
      if (bytesRecvd == buffSize) {
        bytesRecvd = buffSize - 1;
      }
    }

    if (x == startMarker) { 
      bytesRecvd = 0; 
      readInProgress = true;
    }
  }
}

//=============
// testData.append("<LED1,200,200,200>") 
// testData.append("<LED1,0 or 1,DAC0,DAC1>") 
void parseData() {

    // split the data into its parts
    
  char * strtokIndx; // this is used by strtok() as an index
  
  strtokIndx = strtok(inputBuffer,",");      // get the first part - the string
  strcpy(messageFromPC, strtokIndx); // copy it to messageFromPC
  
  strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
  newledonoff = atoi(strtokIndx);     // convert this part to an integer
  
  strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
  newDAC0level = atoi(strtokIndx);     // convert this part to an integer

  strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
  newDAC1level = atoi(strtokIndx);     // convert this part to an integer  

}

//=============

void replyToPC() {

  if (newDataFromPC) {
    newDataFromPC = false;
    Serial.print("<Msg ");
    Serial.print(messageFromPC);
    Serial.print(" onoff ");
    Serial.print(newledonoff);
    Serial.print(" newDAC0level ");
    Serial.print(newDAC0level);
    Serial.print(" newDAC1level ");
    Serial.print(newDAC1level);
    Serial.print(" Time ");
    Serial.print(curMillis >> 9); // divide by 512 is approx = half-seconds
    Serial.println(">");
  }
}

//============

void updateFlashInterval() {

   // this illustrates using different inputs to call different functions
  if (strcmp(messageFromPC, "LED1") == 0) {
     updateLED1();
  }
  
  if (strcmp(messageFromPC, "LED2") == 0) {
     updateLED2();
  }

   if (strcmp(messageFromPC, "LED3") == 0) {
     updateLED3();
  }
  
}

//=============

void updateLED1() {

  if ((newledonoff <= 1) && (newledonoff >= 0)) {
    LEDinterval[0] = newledonoff;
  }
}

//=============

void updateLED2() {

  if ((newledonoff <= 1) && (newledonoff >= 0)) {
    LEDinterval[1] = newledonoff;
  }
}

//=============

void updateLED3() {

  if ((newledonoff <= 1) && (newledonoff >= 0)) {
    LEDinterval[2] = newledonoff;
  }
}
//=============

void flashLEDs() {

  for (byte n = 0; n < numLEDs; n++) {
      if (LEDinterval[n])
       digitalWrite( ledPin[n], HIGH );
       else
       digitalWrite( ledPin[n], LOW );
  }
}

void updateDAC0() {

  if (newDAC0level != DAC0level) {
    DAC0level = newDAC0level;
  }
}

//=============

void updateDAC1() {

  if (newDAC1level != DAC1level) {
    DAC1level = newDAC1level;
  }
}

void flashDACs() {

  analogWrite(DAC0, DAC0level);
  analogWrite(DAC1, DAC1level);
}
